<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Login | Bolemeter Content Management System</title>

    <link href="{{ asset("/css/app.css") }}" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <div class="login_wrapper">
        <div class="x_panel">
          <div class="x_content">
            <div class="form">
              <section class="login_content" style="text-align: left;">
                <form method="POST" action="{{ route('login') }}">
                  <div class="width:100%;" align="center">
                    <img src="{{ asset('/images/logo-bolameter.png') }}" width="125">
                    <h2 style="margin-top: 20px; margin-bottom: 20px;">Sign In</h2>
                  </div>
                  @if(Session::has('action_status'))
                    @if(Session::get('action_status') == "1")
                      <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>Success</strong> {{ Session::get('action_message') }}
                      </div>
                    @else
                      <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>Failed</strong> {{ Session::get('action_message') }}
                      </div>
                    @endif
                  @endif
                  {{ csrf_field() }}
                  <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="text" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus/>
                    @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" class="form-control" name="password" placeholder="Password" required/>
                    @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-success submit">Sign In</button>
                    <a class="reset_pass to_forgot" href="{{ route('password.request') }}">Forgot your password?</a>
                  </div>

                  <div class="clearfix"></div>

                  <div class="separator" align="center">
                    <p class="change_link">New to site?
                      <a href="{{ route('register') }}" class="to_register"> Create Account </a>
                    </p>

                    <div class="clearfix"></div>
                    <br />

                    <div>
                      <p>©{{ date("Y") }} All Rights Reserved. Bolameter Indonesia. Privacy and Terms</p>
                    </div>
                  </div>
                </form>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="{{ asset("/js/app.js") }}"></script>
  </body>
</html>