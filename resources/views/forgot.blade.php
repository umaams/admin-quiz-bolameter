<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Forgot Password | Total Fishery Chain</title>

    <link href="{{ asset("/css/app.css") }}" rel="stylesheet">
  </head>

  <body class="login" style="background-image: url('{{ asset('/images/background.jpg') }}'); background-size: cover;">
    <div>
      <div class="login_wrapper">
        <div class="x_panel">
          <div class="x_content">
            <div class="form">
              <section class="login_content" style="text-align: left;">
                <form method="POST" action="{{ route('password.email') }}">
                  <div class="width:100%;" align="center">
                    <img src="{{ asset('/images/logo-tfc.png') }}">
                    <h2 style="margin-top: 20px; margin-bottom: 20px;">Reset Password</h2>
                  </div>
                  {{ csrf_field() }}
                  <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="text" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus/>
                    @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-success submit">Send Password Reset Link</button>
                  </div>

                  <div class="clearfix"></div>

                  <div class="separator" align="center">

                    <p class="change_link">Already a member ?
                      <a href="{{ url('/login') }}" class="to_register"> Log in </a>
                    </p>

                    <div class="clearfix"></div>
                    <br />

                    <div>
                      <p>©{{ date("Y") }} All Rights Reserved. Kelola Mina Laut Group. Privacy and Terms</p>
                    </div>
                  </div>
                </form>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="{{ asset("/js/app.js") }}"></script>
  </body>
</html>