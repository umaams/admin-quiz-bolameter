@extends('layouts.template')

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="x_panel">
      <div class="x_content">
          <div class="row">
            <div class="col-md-12">
              <form action="{{ route('quiz_category.update',[ 'id' => $quiz_category->id ]) }}" method="post" class="form-horizontal" role="form">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PATCH"/>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                  <label class="control-label col-md-4">Name *</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" name="name" value="{{ $quiz_category->name }}" placeholder="Name" required autofocus/>
                    @if ($errors->has('name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('name') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                  <label class="control-label col-md-4">Description</label>
                  <div class="col-md-8">
                    <textarea name="description" class="form-control">{{ $quiz_category->description }}</textarea>
                    @if ($errors->has('description'))
                      <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group {{ $errors->has('isactive') ? ' has-error' : '' }}">
                  <label class="control-label col-md-4">Status</label>
                  <div class="radio radio-inline">
                    <label>
                      <input type="radio" name="isactive" value="0" @if($quiz_category->isactive == '0') checked @endif>
                      Nonactive
                    </label>
                    <label>
                      <input type="radio" name="isactive" value="1" @if($quiz_category->isactive == '1') checked @endif>
                      Active
                    </label>
                  </div>
                  <div class="col-md-8">
                    @if ($errors->has('isactive'))
                      <span class="help-block">
                          <strong>{{ $errors->first('isactive') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">Image</label>
                  <div class="col-md-8">
                    <div class="input-group">
                      <input id="thumbnail" class="form-control" type="text" name="image_url" value="{{ $quiz_category->image_url }}">
                      <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                          <i class="fa fa-picture-o"></i> Choose
                        </a>
                      </span>
                    </div>
                  <img id="holder" src="{{ asset($quiz_category->image_url) }}" style="margin-top:15px;max-height:100px;">                    
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-10 col-sm-offset-4">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    <a href="{{ route('quiz_category.index') }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
 <script src="{{ asset('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
 <script>
  var domain = "";
  $('#lfm').filemanager('image', {prefix: domain});
 </script>
@endsection