@extends('layouts.template')

@section('content')
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
          <div class="row" style="margin-bottom: 10px;">
            <div class="col-md-12">
              <a href="{{ route('quiz.question.create', ['id' => $quiz_id]) }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> Add New Question</a>
              <a href="{{ route('quiz.question.import', ['id' => $quiz_id]) }}" class="btn btn-default pull-right"><i class="fa fa-upload"></i> Import xls</a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-striped table-hovered">
                  <thead>
                    <tr class="info">
                      <th class="text-center">Title</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Updated at</th>
                      <th class="text-center"></th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
  $('table').DataTable({
    processing: true,
    serverSide: true,
    ajax: '{!! route('datatable.quiz.question', ['id' => $quiz_id]) !!}',
    columns: [
        { data: 'name', name: 'name' },
        { data: 'isactive', name: 'isactive', class: 'text-center' },
        { data: 'updated_at', name: 'updated_at', class: 'text-center' },
        { data: 'action', name: 'action', class: 'text-center', orderable: false, searchable: false }
    ],
    language: { 
     "processing": "<span class='text text-info'><b><i class='fa fa-spinner fa-spin'></i> Loading...</b></span>"
    },
    "order": [[ 2, "desc" ]]
  });
  @if(Session::has('action_status'))
    @if(Session::get('action_status') == "1")
      toastr.success('{{ Session::get('action_message') }}', 'Success');
    @else
      toastr.error('{{ Session::get('action_message') }}', 'Failed');
    @endif
  @endif
</script>
@endsection
