@extends('layouts.template')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_content">
          <div class="row">
            <div class="col-md-12">
              <form action="{{ route('question.store') }}" method="post" class="form-horizontal" role="form">
                {{ csrf_field() }}
                <input type="hidden" name="quiz_id" value="{{ $quiz_id }}">
                <input type="hidden" name="quiz_type" value="{{ $quiz_type }}">
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                  <label class="control-label col-md-2">Title *</label>
                  <div class="col-md-5">
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Question Title" required autofocus/>
                    @if ($errors->has('name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('name') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                @if($quiz_type == 'text')
                <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                  <label class="control-label col-md-2">Description</label>
                  <div class="col-md-10">
                    <textarea name="description" class="form-control" placeholder="Your question text here.." required>{{ old('description') }}</textarea>
                    @if ($errors->has('description'))
                      <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                @endif
                @if($quiz_type == 'image')
                <div class="form-group">
                  <label class="control-label col-md-2">Image</label>
                  <div class="col-md-5">
                    <div class="input-group">
                      <input id="thumbnail" class="form-control" type="text" name="image_url" readonly required>
                      <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                          <i class="fa fa-picture-o"></i> Choose
                        </a>
                      </span>
                    </div>
                  <img id="holder" style="margin-top:15px;max-height:100px;">                    
                  </div>
                </div>
                @endif
                <div class="form-group {{ $errors->has('isactive') ? ' has-error' : '' }}">
                  <label class="control-label col-md-2">Status</label>
                  <div class="radio radio-inline col-md-10">
                    <label>
                      <input type="radio" name="isactive" value="0">
                      Nonactive
                    </label>
                    <label>
                      <input type="radio" name="isactive" value="1" checked>
                      Active
                    </label>
                  </div>
                  <div class="col-md-8">
                    @if ($errors->has('isactive'))
                      <span class="help-block">
                          <strong>{{ $errors->first('isactive') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group">
                <table class="table" style="width:100%;">
                    <tbody>
                        @for($i = 0; $i < 4; $i++)
                        <tr>
                            <td style="width:20%;">
                                <select class="form-control" name="answer_iscorrect[]" required>
                                    <option value="0">Incorrect</option>
                                    <option value="1">Correct</option>
                                </select>
                            </td>
                            <td><textarea class="form-control" name="answer_description[]" placeholder="Answer Text {{ ($i+1) }}" required></textarea></td>
                        </tr>
                        @endfor
                    </tbody>
                </table>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    <a href="{{ route('quiz.question', ['id' => $quiz_id]) }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
 <script src="{{ asset('/vendor/laravel-filemanager/js/lfm.js') }}"></script>
 <script>
  var domain = "";
  $('#lfm').filemanager('image', {prefix: domain});
 </script>
@endsection
