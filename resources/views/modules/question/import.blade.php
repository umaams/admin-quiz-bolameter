@extends('layouts.template')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_content">
          <div class="row">
            <div class="col-md-12">
              <form action="{{ route('question.import.store') }}" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
                {{ csrf_field() }}
                <input type="hidden" name="quiz_id" value="{{ $quiz_id }}">
                <div class="form-group {{ $errors->has('file') ? ' has-error' : '' }}">
                  <label class="control-label col-md-2">Title *</label>
                  <div class="col-md-5">
                    <input type="file" class="form-control" name="file" placeholder="Question Title" required autofocus/>
                    @if ($errors->has('file'))
                      <span class="help-block">
                          <strong>{{ $errors->first('file') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-12">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Import</button>
                    <a href="{{ route('quiz.question', ['id' => $quiz_id]) }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
@endsection
