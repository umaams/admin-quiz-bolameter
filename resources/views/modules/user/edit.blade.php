@extends('layouts.template')

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="x_panel">
      <div class="x_content">
          <div class="row">
            <div class="col-md-12">
              <form action="{{ route('user.update',[ 'id' => $user->id ]) }}" method="post" class="form-horizontal" role="form">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PATCH"/>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                  <label class="control-label col-md-4">Name *</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" name="name" value="{{ $user->name }}" placeholder="Name" required autofocus/>
                    @if ($errors->has('name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('name') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                  <label class="control-label col-md-4">Email *</label>
                  <div class="col-md-8">
                    <input type="email" class="form-control" name="email" value="{{ $user->email }}" placeholder="Email" required/>
                    @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-10 col-sm-offset-4">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    <a href="{{ route('user.index') }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection
