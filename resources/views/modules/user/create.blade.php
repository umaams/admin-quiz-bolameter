@extends('layouts.template')

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="x_panel">
      <div class="x_content">
          <div class="row">
            <div class="col-md-12">
              <form action="{{ route('user.store') }}" method="post" class="form-horizontal" role="form">
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                  <label class="control-label col-md-4">Name *</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus/>
                    @if ($errors->has('name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('name') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                  <label class="control-label col-md-4">Email *</label>
                  <div class="col-md-8">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required/>
                    @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                  <label class="control-label col-md-4">Password *</label>
                  <div class="col-md-8">
                    <input type="password" class="form-control" name="password" placeholder="Min. 6 characters" value="" required/>
                    @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                  <label class="control-label col-md-4">Confirm Password *</label>
                  <div class="col-md-8">
                    <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" value="" required/>
                    @if ($errors->has('password_confirmation'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password_confirmation') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-10 col-sm-offset-4">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection
