<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quiz extends Model
{
    use SoftDeletes;

    protected $table = "quizes";

    public function quiz_category()
    {
    	return $this->belongsTo('App\QuizCategory','quiz_category_id','id');
    }

    public function creator()
    {
    	return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function updater()
    {
    	return $this->belongsTo('App\User', 'updated_by', 'id');
    }

    public function deleter()
    {
    	return $this->belongsTo('App\User', 'deleted_by', 'id');
    }

    public function questions()
    {
        return $this->hasMany('App\Question', 'quiz_id', 'id');
    }
}
