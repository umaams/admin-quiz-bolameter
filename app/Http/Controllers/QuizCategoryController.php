<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuizCategory;
use Auth;
use Validator;
use Yajra\Datatables\Datatables;
use Session;
use Carbon;

class QuizCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "Quiz Category";
        return view('modules.quiz_category.view', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Create New Quiz Category";
        return view('modules.quiz_category.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[ 
            'name' => 'required',
            'isactive' => 'required'
        ]);
        if($validation->fails()){
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }else{
            $quiz_category = new QuizCategory;
            $quiz_category->name = $request->name;
            $quiz_category->description = $request->description;
            $quiz_category->isactive = $request->isactive;
            $quiz_category->image_url = $request->image_url;
            $quiz_category->created_by = Auth::user()->id;
            $quiz_category->updated_by = Auth::user()->id;
            $quiz_category->save();

            Session::flash('action_status', '1');
            Session::flash('action_message', 'Quiz Category is successfully created');
            return redirect()->route('quiz_category.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit Quiz Category";
        $quiz_category = QuizCategory::findOrFail($id);
        return view('modules.quiz_category.edit', compact('title','quiz_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = true;
        $errors = array();
        $validation = Validator::make($request->all(),[ 
            'name' => 'required',
            'isactive' => 'required'
        ]);
        if($validation->fails()){
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }else{
            $quiz_category = QuizCategory::find($id);
            $quiz_category->name = $request->name;
            $quiz_category->description = $request->description;
            $quiz_category->isactive = $request->isactive;
            $quiz_category->image_url = $request->image_url;
            $quiz_category->updated_by = Auth::user()->id;
            $quiz_category->save();

            Session::flash('action_status', '1');
            Session::flash('action_message', 'Quiz Category is successfully updated');
            return redirect()->route('quiz_category.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = true;
        $errors = array();
        QuizCategory::find($id)->delete();
        Session::flash('action_status', '1');
        Session::flash('action_message', 'Quiz Category is successfully deleted');
        return redirect()->route('quiz_category.index');
    }

    public function datatable(Request $request)
    {
        $quiz_categories = QuizCategory::with(['creator', 'updater']);
        return Datatables::of($quiz_categories)
        ->editColumn('created_at', function ($quiz_category) {
            return $quiz_category->created_at->diffForHumans().' by '.$quiz_category->creator->name;
        })
        ->editColumn('updated_at', function ($quiz_category) {
            return $quiz_category->updated_at->diffForHumans().' by '.$quiz_category->updater->name;
        })
        ->editColumn('isactive', function ($quiz_category) {
            return "<span class='label ".(($quiz_category->isactive == '1') ? "label-success" : "label-warning")."'>".(($quiz_category->isactive == '1') ? "Active" : "Nonactive")."</span>";
        })
        ->addColumn('action', function ($quiz_category) {
            $text = "<a href='".route('quiz_category.edit',['id' => $quiz_category->id])."' class='btn btn-success btn-xs'><i class='fa fa-edit'></i> Edit</a>";
            $text.= "<form style='display:inline;' onsubmit='return confirm(\"Do you want to delete this item?\");' method='post' action='".route('quiz_category.destroy', ['id' => $quiz_category->id])."'>
                        <input name='_method' type='hidden' value='DELETE'/>
                        <input name='_token' type='hidden' value='".csrf_token()."'/>
                        <button type='submit' class='btn btn-danger btn-xs'><i class='fa fa-trash-o'></i> Delete</button>
                    </form>";
            return $text;
        })
        ->rawColumns(['isactive','action'])
        ->make(true);
    }
}
