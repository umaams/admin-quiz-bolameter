<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use App\QuizCategory;
use Auth;
use Validator;
use Yajra\Datatables\Datatables;
use Session;
use Carbon;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "Quiz";
        return view('modules.quiz.view', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Create New Quiz";
        $quiz_categories = QuizCategory::all();
        return view('modules.quiz.create', compact('title', 'quiz_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[ 
            'name' => 'required',
            'quiz_category_id' => 'required',
            'isactive' => 'required',
            'type' => 'required',
            'point_per_question' => 'required|numeric|min:0',
            'time_per_question' => 'required|numeric|min:0',
            'question_per_play' => 'required|numeric|min:0'
        ]);
        if($validation->fails()){
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }else{
            $quiz = new Quiz;
            $quiz->name = $request->name;
            $quiz->quiz_category_id = $request->quiz_category_id;
            $quiz->description = $request->description;
            $quiz->type = $request->type;
            $quiz->isactive = $request->isactive;
            $quiz->point_per_question = $request->point_per_question;
            $quiz->time_per_question = $request->time_per_question;
            $quiz->question_per_play = $request->question_per_play;
            $quiz->image_url = $request->image_url;
            $quiz->created_by = Auth::user()->id;
            $quiz->updated_by = Auth::user()->id;
            $quiz->save();

            Session::flash('action_status', '1');
            Session::flash('action_message', 'Quiz is successfully created');
            return redirect()->route('quiz.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit Quiz";
        $quiz_categories = QuizCategory::all();
        $quiz = Quiz::with(['quiz_category'])->findOrFail($id);
        return view('modules.quiz.edit', compact('title', 'quiz', 'quiz_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(),[ 
            'name' => 'required',
            'quiz_category_id' => 'required',
            'point_per_question' => 'required|numeric|min:0',
            'time_per_question' => 'required|numeric|min:0',
            'question_per_play' => 'required|numeric|min:0'
        ]);
        if($validation->fails()){
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }else{
            $quiz = Quiz::find($id);
            $quiz->name = $request->name;
            $quiz->quiz_category_id = $request->quiz_category_id;
            $quiz->description = $request->description;
            if(!$quiz->has('questions')){
                $quiz->type = $request->type;
            }
            $quiz->isactive = $request->isactive;
            $quiz->point_per_question = $request->point_per_question;
            $quiz->time_per_question = $request->time_per_question;
            $quiz->question_per_play = $request->question_per_play;
            $quiz->image_url = $request->image_url;
            $quiz->updated_by = Auth::user()->id;
            $quiz->save();

            Session::flash('action_status', '1');
            Session::flash('action_message', 'Quiz is successfully updated');
            return redirect()->route('quiz.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Quiz::where('id', $id)->update(array('deleted_by' => Auth::user()->id));
        Quiz::find($id)->delete();
        Session::flash('action_status', '1');
        Session::flash('action_message', 'Quiz is successfully deleted');
        return redirect()->route('quiz.index');
    }

    public function datatable(Request $request)
    {
        $quizes = Quiz::with(['quiz_category', 'creator', 'updater']);
        return Datatables::of($quizes)
        ->editColumn('updated_at', function ($quiz) {
            return $quiz->updated_at->diffForHumans().' by '.$quiz->updater->name;
        })
        ->editColumn('isactive', function ($quiz) {
            return "<span class='label ".(($quiz->isactive == '1') ? "label-success" : "label-warning")."'>".(($quiz->isactive == '1') ? "Active" : "Nonactive")."</span>";
        })
        ->addColumn('category_name', function ($quiz) {
            return $quiz->quiz_category->name;
        })
        ->addColumn('action', function ($quiz) {
            $text = "<a href='".route('quiz.edit',['id' => $quiz->id])."' class='btn btn-success btn-xs'><i class='fa fa-edit'></i> Edit</a>";
            $text.= " <a href='".route('quiz.question',['id' => $quiz->id])."' class='btn btn-info btn-xs'><i class='fa fa-edit'></i> Question</a>";
            $text.= "<form style='display:inline;' onsubmit='return confirm(\"Do you want to delete this item?\");' method='post' action='".route('quiz.destroy', ['id' => $quiz->id])."'>
                        <input name='_method' type='hidden' value='DELETE'/>
                        <input name='_token' type='hidden' value='".csrf_token()."'/>
                        <button type='submit' class='btn btn-danger btn-xs'><i class='fa fa-trash-o'></i> Delete</button>
                    </form>";
            return $text;
        })
        ->rawColumns(['isactive','action'])
        ->make(true);
    }

    public function question($id)
    {
        $title = "Question";
        $subtitle = Quiz::find($id)->name;
        $quiz_id = Quiz::find($id)->id;
        return view('modules.question.view', compact('title', 'subtitle', 'quiz_id'));
    }
}
