<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;
use Carbon\Carbon;
use Auth;
use Validator;
use Session;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "User Administrators";
        return view('modules.user.view', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Create New User";
        return view('modules.user.create', compact('title','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name'  => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password'
        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->remember_token = str_random(10);
            $user->save();

            Session::flash('action_status', '1');
            Session::flash('action_message', 'User is successfully created');
            return redirect()->route('user.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit User";
        $user = User::findOrFail($id);
        return view('modules.user.edit', compact('title','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'  => 'required',
            'email' => 'required|email|unique:users,email,'.$id
        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $user = User::findOrFail($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();
            Session::flash('action_status', '1');
            Session::flash('action_message', 'User is successfully updated');
        }
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        Session::flash('action_status', '1');
        Session::flash('action_message', 'User is successfully deleted');
        return redirect()->route('user.index');
    }

    public function datatable(Request $request)
    {
        $users = User::all();
        return Datatables::of($users)
        ->editColumn('created_at', function ($user) {
            return $user->created_at->diffForHumans();
        })
        ->addColumn('action', function ($user) {
            $text = "<a href='".route('user.edit',['id' => $user->id])."' class='btn btn-success btn-xs'><i class='fa fa-edit'></i> Edit</a>";
            $text.= "<form style='display:inline;' onsubmit='return confirm(\"Do you want to delete this item?\");' method='post' action='".route('user.destroy', ['id' => $user->id])."'>
                        <input name='_method' type='hidden' value='DELETE'/>
                        <input name='_token' type='hidden' value='".csrf_token()."'/>
                        <button type='submit' class='btn btn-danger btn-xs'><i class='fa fa-trash-o'></i> Delete</button>
                    </form>";
            return $text;
        })
        ->rawColumns(['roles','action'])
        ->make(true);
    }
}
