<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Quiz;
use App\Answer;
use Auth;
use Validator;
use Yajra\Datatables\Datatables;
use Session;
use Carbon;
use Excel;
use Input;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $title = "Create New Question";
        $subtitle = Quiz::find($id)->name;
        $quiz_id = $id;
        $quiz_type = Quiz::find($id)->type;
        return view('modules.question.create', compact('title', 'subtitle', 'quiz_id', 'quiz_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->quiz_type == 'text') {
            $validation = Validator::make($request->all(),[ 
                'name' => 'required',
                'quiz_id' => 'required',
                'description' => 'required',
                'isactive' => 'required',
                'answer_description' => 'required',
                'answer_iscorrect' => 'required'
            ]);
        } else {
            $validation = Validator::make($request->all(),[ 
                'name' => 'required',
                'quiz_id' => 'required',
                'image_url' => 'required',
                'isactive' => 'required',
                'answer_description' => 'required',
                'answer_iscorrect' => 'required'
            ]);
        }
        if ($validation->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        } else {
            $question = new Question;
            $question->name = $request->name;
            $question->quiz_id = $request->quiz_id;
            if ($request->quiz_type == 'text') {
                $question->description = $request->description;
            } else {
                $question->image_url = $request->image_url;
            }
            $question->isactive = $request->isactive;
            $question->created_by = Auth::user()->id;
            $question->updated_by = Auth::user()->id;
            $question->save();

            for($i = 0; $i < count($request->answer_description); $i++){
                $answer = new Answer;
                $answer->description = $request->answer_description[$i];
                $answer->iscorrect = $request->answer_iscorrect[$i];
                $answer->question_id = $question->id;
                $answer->created_by = Auth::user()->id;
                $answer->updated_by = Auth::user()->id;
                $answer->save();
            }

            Session::flash('action_status', '1');
            Session::flash('action_message', 'Question is successfully created');
            return redirect()->route('quiz.question', ['id' => $request->quiz_id]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit Question";
        $question = Question::with(['answers'])->findOrFail($id);
        return view('modules.question.edit', compact('title', 'question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $quiz_type = Question::find($id)->quiz->type;
        if ($quiz_type == 'text') {
            $validation = Validator::make($request->all(),[ 
                'name' => 'required',
                'description' => 'required',
                'isactive' => 'required',
                'answer_id' => 'required',
                'answer_description' => 'required',
                'answer_iscorrect' => 'required'
            ]);
        } else {
            $validation = Validator::make($request->all(),[ 
                'name' => 'required',
                'image_url' => 'required',
                'isactive' => 'required',
                'answer_id' => 'required',
                'answer_description' => 'required',
                'answer_iscorrect' => 'required'
            ]);
        }
        if ($validation->fails()) {
            return redirect()->back()
            ->withErrors($validation)
            ->withInput();
        } else {
            $question = Question::find($id);
            $question->name = $request->name;
            $question->description = $request->description;
            $question->isactive = $request->isactive;
            $question->updated_by = Auth::user()->id;
            $question->save();

            for($i = 0; $i < count($request->answer_id); $i++){
                $answer = Answer::find($request->answer_id[$i]);
                $answer->description = $request->answer_description[$i];
                $answer->iscorrect = $request->answer_iscorrect[$i];
                $answer->updated_by = Auth::user()->id;
                $answer->save();
            }

            Session::flash('action_status', '1');
            Session::flash('action_message', 'Question is successfully updated');
            return redirect()->route('quiz.question', ['id' => $question->quiz_id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);
        $question->deleted_by = Auth::user()->id;
        $question->save();
        $quiz_id = $question->quiz_id;
        Question::find($id)->delete();
        Session::flash('action_status', '1');
        Session::flash('action_message', 'Question is successfully deleted');
        return redirect()->route('quiz.question', ['id' => $quiz_id]);
    }

    public function datatable($id, Request $request)
    {
        $questions = Question::with(['quiz', 'creator', 'updater']);
        return Datatables::of($questions)
        ->editColumn('updated_at', function ($question) {
            return $question->updated_at->diffForHumans().' by '.$question->updater->name;
        })
        ->editColumn('isactive', function ($question) {
            return "<span class='label ".(($question->isactive == '1') ? "label-success" : "label-warning")."'>".(($question->isactive == '1') ? "Active" : "Nonactive")."</span>";
        })
        ->addColumn('action', function ($question) {
            $text = "<a href='".route('question.edit',['id' => $question->id])."' class='btn btn-success btn-xs'><i class='fa fa-edit'></i> Edit</a>";
            $text.= "<form style='display:inline;' onsubmit='return confirm(\"Do you want to delete this item?\");' method='post' action='".route('question.destroy', ['id' => $question->id])."'>
                        <input name='_method' type='hidden' value='DELETE'/>
                        <input name='_token' type='hidden' value='".csrf_token()."'/>
                        <button type='submit' class='btn btn-danger btn-xs'><i class='fa fa-trash-o'></i> Delete</button>
                    </form>";
            return $text;
        })
        ->rawColumns(['isactive','action'])
        ->make(true);
    }

    public function import($id)
    {
        $title = "Import Question";
        $subtitle = Quiz::find($id)->name;
        $quiz_id = $id;
        return view('modules.question.import', compact('title', 'subtitle', 'quiz_id'));
    }

    public function import_store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'quiz_id' => 'required', 
            'file' => 'required'
        ]);
        if ($validation->fails()) {
            return redirect()->back()
            ->withErrors($validation)
            ->withInput();
        } else {
            $path = $request->file('file')->getRealPath();
            Excel::load($path, function($reader) {
                $results = $reader->toArray()[0];
                foreach ($results as $item) {
                    var_dump($item);
                }
            });
        }
    }


    
}
