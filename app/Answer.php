<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{

    protected $table = "answers";

    public function quiz()
    {
    	return $this->belongsTo('App\Question','question_id','id');
    }

    public function creator()
    {
    	return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function updater()
    {
    	return $this->belongsTo('App\User', 'updated_by', 'id');
    }

    public function deleter()
    {
    	return $this->belongsTo('App\User', 'deleted_by', 'id');
    }
}
