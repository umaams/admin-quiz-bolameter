<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;

    protected $table = "questions";

    public function quiz()
    {
    	return $this->belongsTo('App\Quiz', 'quiz_id', 'id');
    }

    public function answers()
    {
        return $this->hasMany('App\Answer', 'question_id', 'id');
    }

    public function creator()
    {
    	return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function updater()
    {
    	return $this->belongsTo('App\User', 'updated_by', 'id');
    }

    public function deleter()
    {
    	return $this->belongsTo('App\User', 'deleted_by', 'id');
    }
}
