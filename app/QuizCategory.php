<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuizCategory extends Model
{
    use SoftDeletes;

    protected $table = "quiz_categories";

    public function creator()
    {
    	return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function updater()
    {
    	return $this->belongsTo('App\User', 'updated_by', 'id');
    }

    public function deleter()
    {
    	return $this->belongsTo('App\User', 'deleted_by', 'id');
    }
}
