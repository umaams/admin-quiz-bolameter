<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['middleware' => ['auth']], function () {
	Route::get('/home', 'HomeController@index')->name('home');

	Route::get('/user', 'UserController@index')->name('user.index');
	Route::get('/user/create', 'UserController@create')->name('user.create');
	Route::post('/user', 'UserController@store')->name('user.store');
	Route::get('/user/{id}', 'UserController@edit')->name('user.edit');
	Route::patch('/user/{id}', 'UserController@update')->name('user.update');
	Route::delete('/user/{id}', 'UserController@destroy')->name('user.destroy');

	Route::get('/quiz_category', 'QuizCategoryController@index')->name('quiz_category.index');
	Route::get('/quiz_category/create', 'QuizCategoryController@create')->name('quiz_category.create');
	Route::post('/quiz_category', 'QuizCategoryController@store')->name('quiz_category.store');
	Route::get('/quiz_category/{id}', 'QuizCategoryController@edit')->name('quiz_category.edit');
	Route::patch('/quiz_category/{id}', 'QuizCategoryController@update')->name('quiz_category.update');
	Route::delete('/quiz_category/{id}', 'QuizCategoryController@destroy')->name('quiz_category.destroy');

	Route::get('/quiz', 'QuizController@index')->name('quiz.index');
	Route::get('/quiz/create', 'QuizController@create')->name('quiz.create');
	Route::get('/quiz/{id}/question/create', 'QuestionController@create')->name('quiz.question.create');
	Route::get('/quiz/{id}/question/import', 'QuestionController@import')->name('quiz.question.import');
	Route::post('/quiz', 'QuizController@store')->name('quiz.store');
	Route::get('/quiz/{id}/question', 'QuizController@question')->name('quiz.question');	
	Route::get('/quiz/{id}', 'QuizController@edit')->name('quiz.edit');
	Route::patch('/quiz/{id}', 'QuizController@update')->name('quiz.update');
	Route::delete('/quiz/{id}', 'QuizController@destroy')->name('quiz.destroy');

	Route::post('/question', 'QuestionController@store')->name('question.store');
	Route::post('/question/import', 'QuestionController@import_store')->name('question.import.store');
	Route::get('/question/{id}', 'QuestionController@edit')->name('question.edit');
	Route::patch('/question/{id}', 'QuestionController@update')->name('question.update');
	Route::delete('/question/{id}', 'QuestionController@destroy')->name('question.destroy');

	Route::prefix('datatable')->group(function () {
		Route::get('/user', 'UserController@datatable')->name('datatable.user');
		Route::get('/quiz_category', 'QuizCategoryController@datatable')->name('datatable.quiz_category');
		Route::get('/quiz', 'QuizController@datatable')->name('datatable.quiz');
		Route::get('/quiz/{id}/question', 'QuestionController@datatable')->name('datatable.quiz.question');
	});
});
