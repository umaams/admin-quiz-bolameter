let mix = require('laravel-mix');
mix.webpackConfig({
    node: {
      fs: "empty"
    },
    resolve: {
        alias: {
            "handlebars" : "handlebars/dist/handlebars.js"
        }
    },
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.autoload({
    jquery: ['$', 'window.jQuery',"jQuery","window.$","jquery","window.jquery"]
}).js([
	'resources/assets/js/app.js',
	'resources/assets/js/custom.js'
], 'public/js');
mix.combine([
    'node_modules/bootstrap/dist/css/bootstrap.min.css',
    'node_modules/font-awesome/css/font-awesome.min.css',
    'node_modules/datatables.net-bs/css/dataTables.bootstrap.css',
    'node_modules/nprogress/nprogress.css',
    'node_modules/animate.css/animate.min.css',
    'node_modules/toastr/build/toastr.min.css',
    'resources/assets/css/custom.css'
], 'public/css/app.css');
